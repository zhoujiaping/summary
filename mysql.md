# mysql篇
## mysql架构
简述MySQL的架构
MySQL可以分为应用层,逻辑层,数据库引擎层,物理层。

应用层：负责和客户端，响应客户端请求，建立连接，返回数据。

逻辑层：包括SQK接口，解析器，优化器，Cache与buffer。

数据库引擎层：有常见的MyISAM,InnoDB等等。

物理层：负责文件存储，日志等等。



## 索引
聚集索引、辅助索引、覆盖索引、联合索引、索引下推

第26期：索引设计（索引下推） https://segmentfault.com/a/1190000039869289



## innodb事务实现
深入学习MySQL事务：ACID特性的实现原理 https://www.cnblogs.com/kismetv/p/10331633.html

MySQL · 引擎特性 · InnoDB 事务系统 http://mysql.taobao.org/monthly/2017/12/01/

MySQL InnoDB MVCC实现 https://zhuanlan.zhihu.com/p/40208895

## 索引实现
b+树
锁
存储引擎
innodb事务实现
	锁、mvcc、redolog、undolog
	事务的原子性是通过undolog来实现的
	事务的持久性性是通过redolog来实现的
	事务的隔离性是通过(读写锁+MVCC)来实现的
	而事务的终极大 boss 一致性是通过原子性，持久性，隔离性来实现的！！！
redo log —— MySQL宕机时数据不丢失的原理 https://blog.csdn.net/qq_34436819/article/details/105664256
一文深入理解mysql https://blog.csdn.net/zhangzhen02/article/details/107152588

mvcc
轻松理解MYSQL MVCC 实现机制 https://www.cnblogs.com/shoshana-kong/p/11244612.html

## binlog和redolog
MySQL 日志系统之 redo log 和 binlog
https://www.huaweicloud.com/articles/3d4508df9e2945703fbebff812f4541b.html
## 其他
一条SQL语句在MySQL中执行过程全解析 https://blog.csdn.net/weter_drop/article/details/93386581

数据库 OLAP、OLTP的介绍和比较 https://www.huaweicloud.com/articles/7b12de6f47d9b1084129e077e53ca7ff.html

innodb四大特性 https://note.youdao.com/ynoteshare1/index.html?id=a6004953a0a7c80073ac74d8e76f1ebd&type=note

MySQL单表数据不要超过500万行

分库分区分表

读写分离

千万级数据量表设计

执行计划

备份恢复

## 主从架构、数据同步
MySQL主从模式的数据一致性
https://www.jianshu.com/p/790a158d9eb3



调度任务轮询分页策略：  
按id升序排序，每次把上次的最大的id带过来作为查询条件，避免出现类似limit 100000 offset 100的情况。  
例如：select * from xxx where id>#{prevMaxId} and create_time>#{xx} and status = #{status};


【图文详解】MySQL系列之redo log、undo log和binlog详解
https://cloud.tencent.com/developer/article/1801920

