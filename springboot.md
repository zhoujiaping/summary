# springboot篇
脚手架 https://start.aliyun.com/bootstrap.html
SpringBoot 总结：核心功能和优缺点 https://blog.csdn.net/weixin_47340771/article/details/107158619
@Import注解的作用 https://blog.csdn.net/mamamalululu00000000/article/details/86711079
简单讲讲@SpringBootApplication https://www.jianshu.com/p/39ee4f98575c
SpringBoot自动配置原理 https://zhuanlan.zhihu.com/p/55637237
SpringBoot启动机制（starter机制）核心原理详解 https://www.jianshu.com/p/d166d935217b
SpringBoot应用篇（一）：自定义starter https://www.cnblogs.com/hello-shf/p/10864977.html
自动装配原理
	核心原理，就是@SpringBootApplication上面有个注解@EnableAutoConfiguration，
	然后@EnableAutoConfiguration上有个注解@Import(AutoConfigurationImportSelector.class)
	这样AutoConfigurationImportSelector的实例会被注册到spring，ImportSelector有个方法返回配置类名称的集合，告诉spring需要加载哪些配置类。
	该方法中，通过SpringFactoriesLoader#loadFactoryNames加载配置类集合。
	loadFactoryNames方法会扫描所有的jar，读取META—INF/spring.factories的内容，过滤掉不满足条件，返回剩下的配置类集合。
自定义starter

