装饰器模式（静态代理）
Cache 缓存接口
void putObject(Object key, Object value);
Object getObject(Object key);

PerpetualCache 永久缓存，缓存到一个hashmap中
BlockingCache 阻塞缓存，读取数据时加锁，阻塞其他线程。存入数据后释放锁。主要作用是防止高并发时在缓存中找不到数据导致穿透到数据库。
  private final Cache delegate;
  private final ConcurrentHashMap<Object, ReentrantLock> locks;
FifoCache 先进先出缓存
  private final Cache delegate;
  private Deque<Object> keyList;
LoggingCache 记录命中信息的缓存
  private Cache delegate;
  protected int requests = 0;请求数
  protected int hits = 0;命中数
LruCache 使用最近最少访问算法的缓存。这里实际上只计算了最近，没有计算最少。
基于LinkedHashMap实现Lru缓存，只需要继承它，并重写removeEldestEntry方法。
  private final Cache delegate;
  private Map<Object, Object> keyMap;
  private Object eldestKey;
  public void setSize(final int size) {
    keyMap = new LinkedHashMap<Object, Object>(size, .75F, true) {
      private static final long serialVersionUID = 4267176411845948333L;

      @Override
      protected boolean removeEldestEntry(Map.Entry<Object, Object> eldest) {
        boolean tooBig = size() > size;
        if (tooBig) {
          eldestKey = eldest.getKey();
        }
        return tooBig;
      }
    };
  }
ScheduledCache 可定时调度的缓存。定时清除缓存中所有内容
  private Cache delegate;
  protected long clearInterval;//清除缓存的间隔
  protected long lastClear;//最后一次清除缓存的时间
SerializedCache 支持对象序列化的缓存。往缓存中添加key-value时，添加的是value的序列化结果，而不是value本身。
  private Cache delegate;
  private byte[] serialize(Serializable value);
  private Serializable deserialize(byte[] value);
SoftCache 支持软引用的缓存。
首先介绍一下java对象的引用类型。不同的引用类型决定对象的生命周期，gc对不同的引用类型采用不同的回收策略。
强引用，只要引用存在，垃圾回收器永远不会回收。
软引用，非必须引用，内存溢出之前进行回收。
弱引用，下一次垃圾回收时回收。
虚引用，垃圾回收时回收，无法通过引用取到对象值。
  private final Deque<Object> hardLinksToAvoidGarbageCollection;//使用一个队列，保存对象的强引用防止被gc回收
  private final ReferenceQueue<Object> queueOfGarbageCollectedEntries;//引用队列，当被垃圾收集器回收时，会将软引用对象放入此队列
  private final Cache delegate;
  private int numberOfHardLinks;
WeakCache 支持弱引用的缓存
  private final Deque<Object> hardLinksToAvoidGarbageCollection;
  private final ReferenceQueue<Object> queueOfGarbageCollectedEntries;
  private final Cache delegate;
  private int numberOfHardLinks;
SynchronizedCache 线程安全的缓存。put、get、clear、remove、getSize方法都用synchronized修饰。
  public synchronized void putObject(Object key, Object object)
TransactionalCache 支持事务的缓存。
mybatis一级缓存：会话级缓存
mybatis二级缓存：应用级缓存
  private Cache delegate;//二级缓存
  private Map<Object, Object> entriesToAddOnCommit;//一级缓存，事务提交后将些内容添加到二级缓存
  private Set<Object> entriesMissedInCache;//访问过程中二级缓存中不存在的key
  public Object getObject(Object key);从二级缓存读取内容
  

工厂模式（TransactionFactory、SqlSessionFactory、ObjectFactory、
ReflectorFactory、MapperProxyFactory、
DataSourceFactory、ProxyFactory、ObjectWrapperFactory、LogFactory、
ExceptionFactory）
要把jdbc事务换成spring事务，只需要在一处将JdbcTransactionFactory替换成SpringManagedTransactionFactory。
TransactionFactory
--JdbcTransactionFactory
--SpringManagedTransactionFactory
public Transaction newTransaction(Connection conn);
public Transaction newTransaction(DataSource dataSource, TransactionIsolationLevel level, boolean autoCommit);

动态代理模式。在mybatis中一个典型用法就是将java方法映射为xml的标签。
也就是用java语言定义接口，用另一门语言定义实现。可以充分利用静态语言和动态语言各自的优势。
比如我们可以使用代理模式，配合dubbo/hessian过滤器，将远程调用替换为本地调用，然后本地调用使用动态语言，替换的策略通过动态语言配置。
那么将极大的方便单元测试，甚至对上线验证也能够提供非常大的灵活性。
MapperProxyFactory
  protected T newInstance(MapperProxy<T> mapperProxy) {
    return (T) Proxy.newProxyInstance(mapperInterface.getClassLoader(), new Class[] { mapperInterface }, mapperProxy);
  }

  public T newInstance(SqlSession sqlSession) {
    final MapperProxy<T> mapperProxy = new MapperProxy<T>(sqlSession, mapperInterface, methodCache);
    return newInstance(mapperProxy);
  }
建造者模式 比如一个对象o，有f1,f2,f3...f10共10个属性。
创建o的过程比较复杂，需要在a方法中设置f1,f2,f3，
在b方法中设置f4,f5,f6,在c方法中设置f7,f8,f9,f10。这时候可以用建造者模式。
builder有三个方法，buildPart1，buildPart2，buildPart3。

策略模式
RoutingStatementHandler同时用到了装饰器模式
根据不同的StatementType，选择具体的StatementHandler
包括SimpleStatementHandler、PreparedStatementHandler、CallableStatementHandler。
比如经营贷还款，可以用一个入口，内部使用提前全额还款、逾期还款、正常还款三个策略。

适配器模式
自己定义了一个日志接口，然后适配log4j、slf4j、stdout等各种日志实现。
好处是将框架的核心代码和具体的日志实现解耦。

单例模式
LogFactory

组合模式
SqlNode一般用于树形结构

模板方法模式
BaseExecutor抽象类，定义一些通用实现（不变的部分），定义一些抽象方法（变化的部分）。将变化与不变的部分分离，
子类实现变化的部分。
类似的例子还有BaseTypeHandler

迭代器模式
PropertyTokenizer
比如#{user.creditApplyList[0].effectTime},第一个PropertyTokenizer处理user.creditApplyList[0].effectTime，
下一个PropertyTokenizer处理creditApplyList[0].effectTime，下一个PropertyTokenizer处理effectTime。
实际应用，比如系统中有很多跑批任务，跑批需要查询列表（客户列表、或者借据列表等），
可以封装一个迭代器，方便各个跑批任务使用，简化代码，减少bug发生的几率。


责任链模式
使多个对象都有机会处理请求，从而避免请求的发送者和接受者之间的耦合关系， 将这个对象连成一条链，并沿着这条链传递该请求，直到有一个对象处理他为止。
mybatis的插件
和servlet的filter，dubbo的filter实现方式都不相同。

面向接口编程
TransactionFactory 有mybatis自己的实现，也有spring的实现。
这样可以非常容易切换为spring的事务管理。
SessionFactory


