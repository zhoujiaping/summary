# mybatis篇
mybatis一级缓存二级缓存 https://www.cnblogs.com/happyflyingpig/p/7739749.html
启动流程
	读取配置
	->解析生成Configuration对象（注册mapper、解析sql、注册plugin、typehandler等）
	->根据Configuration创建SqlSessionFactory。
执行流程
	获取mapper（MapperProxyFactory创建的jdk代理对象MapperProxy）
	->执行mapper接口方法
	->执行MapperProxy#invoke
	->MapperMethod#execute
	->sqlSession#insert/update等
	->executor#update（执行executor插件），使用了装饰器模式，通过CachingExecutor装饰Executor实现应用级缓存功能。
		BaseExecutor中实现了会话级缓存功能。
	->StatementHandler#update（执行StatementHandler插件）
核心组件
	SqlSession、Executor、StatementHandler、ResultSetHandler
插件机制
	Mybatis插件能够对则四大对象进行拦截。
	启动时解析配置文件，在获取四大对象时，通过动态代理实现责任链模式进行拦截。
缓存机制
整合spring
	spring扫描并注册mapper；spring提供SqlSessionFactory的实现；spring提供TransactionFactory的实现。
事务
	定义了Transaction、TransactionFactory接口，有JdbcTransaction、JdbcTransactionFactory实现，
	和ManagedTransaction、ManagedTransactionFactory实现（mybatis并没有实现，交给容器实现）。

聊聊MyBatis缓存机制 https://tech.meituan.com/2018/01/19/mybatis-cache.html
