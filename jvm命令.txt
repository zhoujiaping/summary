jinfo
jinfo可以查看设置的jvm的信息，

jinfo -flag MaxHeapSize [pid]  能够查看最大堆内存

jinfo -flag ThreadStackSize [pid] 



jinfo -flags [pid]

jinfo -flag UseConcMarkSweepGC [pid]

jinfo -flag UseG1GC [pid]

jinfo -flag UseParallelGC [pid]

jvm监控


jstat gc情况
https://www.cnblogs.com/yjd_hycf_space/p/7755633.html

定位死循环
jstack

定位死锁
jps
jstack
Found one Java-level deadlock:

查看JVM启动时的参数
jcmd pid VM.flags

可视化
jvisualvm.exe

查看堆使用情况
jmap –heap pid
MaxNewSize=EdenSpace+From Space+To Space
NewRatio=OldSize/MaxNewSize=2
SurvivorRatio=MaxNewSize/From Space
一般FromSpace=To Space，但如果不能取整也会有较小的差异

jstack pid


top -H -p pid
-H 一行显示一个线程（不加的话一行显示一个进程）

定位java繁忙的线程
https://hub.fastgit.org/oldratlee/useful-scripts/blob/master/docs/java.md

定位jvm内存溢出
https://blog.csdn.net/xc123_java/article/details/89156942
1 启动时添加参数
-XX:+HeapDumpOnOutOfMemoryError
-XX:HeapDumpPath=/opt/jvmdump
2 使用jmap命令收集
通过jmap -dump:live,format=b,file=/opt/jvm/dump.hprof pid。
3 使用工具MAT(MemoryAnalyzer)进行分析

