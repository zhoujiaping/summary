# nginx篇
Nginx原理详解 https://blog.csdn.net/wangyunzhong123/article/details/77469653
nginx负载均衡的5种策略及原理 https://blog.csdn.net/qq_35119422/article/details/81505732
nginx架构
	Nginx分为单工作进程和多工作进程两种模式，单进程多线程 和 多进程单线程。

多进程模式
	启动时会创建master进程，然后会fork多个worker进程。worker进程都监听服务端口的accept事件和io事件，抢夺任务。
	master和worker都是单线程的。

处理请求的过程
	首先，每个worker进程都是从master进程fork过来，在master进程里面，先建立好需要listen的socket（listenfd）之后，然后再fork出多个worker进程。所有worker进程的listenfd会在新连接到来时变得可读，为保证只有一个进程处理该连接，所有worker进程在注册listenfd读事件前抢accept_mutex，抢到互斥锁的那个进程注册listenfd读事件，在读事件里调用accept接受该连接。

	当一个worker进程在accept这个连接之后，就开始读取请求，解析请求，处理请求，产生数据后，再返回给客户端，最后才断开连接，这样一个完整的请求就是这样的了。
多进程的好处
	方便实现nginx -s reload热部署。
	各进程之间相互独立，实现了隔离。
nginx核心模块
	HTTP模块、EVENT模块和MAIL模块

nginx高可用 
	keepalived
nginx负载均衡策略
	轮询（默认）
	指定权重
	IP绑定 ip_hash
	fair（第三方）
	url_hash（第三方）
nginx实现动静分离
	sendfile实现静态文件快速响应

