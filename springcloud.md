# springcloud篇
Spring Cloud 优雅下线以及灰度发布
https://blog.csdn.net/qq_35246620/article/details/109166722

超详细的SpringCloud底层原理 https://blog.csdn.net/mkmkmkhh/article/details/101996991

Netflix Eureka
	注册中心
作为架构师的我，微服务注册中心该如何选型
https://my.oschina.net/u/4344310/blog/4315835
Ribbon
	客户端负载均衡器
Fegin
	接口调用组件
Hystrix
	容错管理工具。服务降级、服务熔断、线程和信号隔离、请求缓存、请求合并以及服务监控等
Zuul
	API网关。动态路由、限流。
（单机的限流可以使用guava，也可以自己基于redis+lua通过令牌桶或者漏斗算法实现）
Spring Cloud Sleuth+zipkin
	链路追踪
Spring Cloud Bus
	事件、消息总线，用于在集群（例如，配置变化事件）中传播状态变化，可与Spring Cloud Config联合实现热部署。
Spring Cloud Task
	提供云端计划任务管理、任务调度。

