# jvm篇
Java虚拟机（JVM）你只要看这一篇就够了！
https://blog.csdn.net/qq_41701956/article/details/81664921

JVM调优总结
https://www.cnblogs.com/andy-zhou/p/5327288.html

jvm常用的命令行工具
https://blog.csdn.net/fst438060684/article/details/82317060

阿里巴巴开源性能监控神器Arthas jvm
https://www.cnblogs.com/shihaiming/p/11388201.html

类加载机制
	双亲委托机制
	自定义类加载器

简介JVM的Parallel Scavenge及Parallel Old垃圾收集器
https://blog.csdn.net/u010798968/article/details/72867690
Parallel Scavenge 使用复制算法
Parallel Old 使用标记-整理算法


JVM 字符串常量池的垃圾回收
https://www.jianshu.com/p/2dc4c3dd5376

线上FullGC频繁的排查
https://developer.aliyun.com/article/610124
多久算频繁？根据具体业务判断，一般几天一次吧。
可能原因：
System.gc()调用
jmap命令触发
内存分配太小
内存分配不合理
内存泄露（代码原因）

定位：
先看内存分配
再看gc统计数据
再dump内存定位内存泄露

happen-before原则
JMM可以通过happens-before关系向程序员提供跨线程的内存可见性保证（如果A线程的写操作a与B线程的读操作b之间存在happens-before关系，尽管a操作和b操作在不同的线程中执行，但JMM向程序员保证a操作将对b操作可见）。


