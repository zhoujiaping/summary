# netty
一文理解Netty模型架构 https://blog.csdn.net/u011447614/article/details/83615876
netty vs jdk的nio
整体架构
核心组件
	Channel、ChannelPipline、ByteBuf、EventLoop、EventLoopGroup、ChannelHandler、ChannelFuture、Selector、ChannelHandlerContext
linux五种io模型
	阻塞io模型、非阻塞io模型、多路复用模型、信号函数模型、异步io模型
编码器/解码器
高性能之道
	基于nio、高效的reactor线程模型、无锁化的串行设计、高性能序列化框架、零拷贝、内存池、灵活的tcp配置能力
tcp粘包拆包
线程模型
	单Reactor单线程、单Reactor多线程、主从Reactor多线程
可靠性
	链路有效性检测、reactor线程的保护、内存保护、流量整形、优雅停机

时间轮算法
类似于手表，有秒针分针时针，秒针转一圈，分针转一格。  
然后安排任务的时候，先计算下一次执行的时间，将任务挂到对应的位置上，等到针转到这个位置，就把任务拿出来，再判断低一级的指针是否到达位置，如果没到，就再挂到低一级时间轮的位置上。  
比如有年、月、日、时、分、秒这几个时间轮，现在时间是2022-05-19 01:17:00，安排一个任务在2022-05-19 02:18:00执行。  
先判断year轮是否到达，现在是的，所以再判断month轮是否到达，现在是的，再判断day轮是否到达，现在是的，再判断hour轮是否到达，现在还没到，所以将任务挂在hour轮的第二个位置。等到2022-05-19 02:00:00到达的时候，从这个位置取出任务，判断minute轮是否到达，没到，就将任务挂到minute轮的第18个位置。等到2022-05-19 02:18:00到达的时候，取出任务，判断second轮是否到达，是的，于是执行它。  
对于需要重复执行的任务，可以再次计算下次执行的时间，将任务挂到对应的位置上。
https://cloud.tencent.com/developer/article/1815722
