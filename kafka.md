# kafka篇
Kafka的生成者、消费者、broker的基本概念
https://chenchenchen.blog.csdn.net/article/details/90231607
Kafka史上最详细原理总结
https://blog.csdn.net/u013573133/article/details/48142677
kafka保证数据一致性的isr机制
https://blog.csdn.net/qq_37502106/article/details/80271800
终于知道Kafka为什么这么快了！
https://zhuanlan.zhihu.com/p/183808742?utm_source=wechat_timeline
	1 利用 Partition 实现并行处理
	2 顺序写磁盘
	3 充分利用 Page Cache
	4 零拷贝技术
		DMA copy
		使用 mmap 内存文件映射
		sendfile
	5 批处理
	6 数据压缩
Kafka 基本原理（8000 字小结） 
https://zhuanlan.zhihu.com/p/87987916

kafka消息丢失（通过配置策略，可以防止消息丢失，但是会降低吞吐）

Kafka之消息传输 http://matt33.com/2016/03/09/kafka-transmit/

消息积压  
    单个如果消费者消费能力不足，可以增加consumer group中消费者数量，某个消费者消费不过来的时候，由同消费者组的其他消费者消费。
    （每个分区只能由同一个消费组内的一个consumer来消费。）  
    
消息乱序  
    可以通过合理的规划设计 Kafka 的配置和方法来避免消息在通过 Kafka 后乱序的产生，只需要遵循以下原则即可：对于需要确保顺序的一条消息流，发送到同一个 partition 上去。
重复消息  
    业务接口实现幂等
