# java并发编程
基础三件套 volatile、Unsafe#cas、Unsafe#park/unpark。
volatile
	线程间变量可见性、防止指令重排序。
Unsafe#cas
	自旋锁
Unsafe#park/unpark
	线程挂起和恢复，与线程是否拥有锁无关。	
基于Unsafe#park/unpark，实现了locksupport，用于线程的挂起和恢复。
aqs、automic*
aqs
	抽象队列同步器
Lock、Condition、ReentrantLock
ConcurrentHashMap、DelayQueue...

synchronized Object对象头（监视器-等待队列和条件队列）、Object#wait/nofity/notifyAll
“对象头(object header)”里知多少？ https://zhuanlan.zhihu.com/p/124278272

线程状态转换图、线程切换
线程状态转换图及各部分介绍 https://blog.csdn.net/sspudding/article/details/89220595
死磕ThreadPoolExecutor线程池 https://blog.csdn.net/rothchil/article/details/111993064
线程池关闭shutdown和awaitTermination，大循环中判断interrupted


内存一致性协议  
主存、工作内存、happenes-before原则  

AQS
透彻理解Java并发的等待队列——Condition  
https://juejin.cn/post/6844903735265771527


