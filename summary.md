# 编程语言
- java
  list、set、map
  juc
  aqs
  无锁数据结构
  线程池
  连接池

- groovy
  jvm上的js
- nodejs
  灵活，核心小
- python
  简单，表达能力强
- kotlin
  现代化编程语言
- clojure
  宏、元编程、设计优雅
- haskell
  函数式
- go
  goroutine
- scala
  多范式、完备的类型系统

函数式编程如何控制副作用


# 工具
svn、git、postman、jmeter、maven、gradle、arthas、markdown、

# 框架、库
mybatis、fastjson、gson、spock、guava、jaassist、bytebuddy、asm、apache httpclient、
netty、dubbo、spring

# 中间件
kafka、zookeeper


# spring、springboot、springcloud

# jvm

# nginx
nginx架构设计
负载均衡策略


复习
mysql、redis、kafka、spring、dubbo、mybatis

spring bean生命周期
springboot自动配置原理
spring事务传播
spring循环依赖




