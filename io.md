系统调用、线程切换、用户空间、内核空间、dma、零拷贝、内存映射文件、sendfile

linux五种io模型
- 同步模型（synchronous IO）
- 非阻塞IO（non-blocking IO）
- 多路复用IO（multiplexing IO）
- 信号驱动式IO（signal-driven IO）
- 异步IO（asynchronous IO）
