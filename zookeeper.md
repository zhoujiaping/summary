# zookeeper篇
这可能是把ZooKeeper概念讲的最清楚的一篇文章
https://blog.csdn.net/u011277123/article/details/82683523
一文了解Zookeeper
https://blog.csdn.net/weixin_38073885/article/details/89577103

如此火爆的ZooKeeper，到底如何选主？
https://www.heapdump.cn/article/379078

脑裂、惊群、选主耗时、网络波动敏感、zab算法


脑裂：zk由于有过半机制，极大的降低了脑裂的可能性

zk原生客户端不好用：用curator

zk的顺序一致性是针对同一个客户端而言的（实际上是针对同一个会话id而言的）


zk选主比较耗时，选举流程通常耗时30到120秒。期间服务不可用。
https://www.zhihu.com/question/42931473

由于zookeeper对于网络隔离的极度敏感，导致zookeeper对于网络的任何风吹草动都会做出激烈反应。
这使得zookeeper的‘不可用’时间比较多，我们不能让zookeeper的‘不可用’，变成系统的不可用。

