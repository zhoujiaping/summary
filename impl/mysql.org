实现rowNo
set @row_no := 0;
select
@row_no:=@row_no+1,
t.*
from user t
where ...;


实现分组
set @row_no := 0;
set @group_id := 0;
set @dept := null;
select
@row_no:=@row_no+1,
if(@dept=t.dept,@group_id,@group_id:=@group_id+1) as group_id,
t.*
from user t
where ...
order by t.dept
;

原来sql可以这么玩，定义一组变量，然后在select里面取值、赋值，实现循环遍历的效果。
可以结合排序实现分组功能。
可以实现窗口函数功能。
不过可用的数据结构不多，可以定义数组，没有set、hashmap、treemap等。
一般不建议这么写，特殊情况可以用这一招。


sql中in条件很多值会不会影响性能？
一般数据库会进行优化，比如使用二分法查找，或者使用hash表。所以in条件的值很多一般只需要关注影响sql语句大小。