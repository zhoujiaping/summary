项目使用微服务架构

服务A依赖服务B，开发环境中经常需要发布服务。
服务B每次发布都影响服务A的使用。
（开发环境没有部署多节点~~~囧，别问，问就是成本）

所以为了提高服务A的可用性，需要一些解决办法。

服务A使用feign调用服务B

方案：
服务A使用aop，拦截对服务B的调用（读数据并且非频繁更新数据的接口），对调用结果缓存。
如果服务B发版，调用失败，就从缓存获取结果。

伪代码如下
function doAround(joinpoint){
    //缓存前缀+租户id+方法签名+方法参数(json)
    //如果json过长，就md5一下
    let cacheKey = getCacheKey(joinpoint)
    try{
        let result = joinpoint.proceed()
        let json = JSON.stringify(result)
        runAsync(()=>{
            redis.set(cacheKey,json,'PX',2hour)
        })
    }cache(e){
        try{
            let json = redis.get(cacheKey)
        }catch(e2){
            e2.suppress = e
            throw e2
        }
        if(json == null){
            throw e
        }
        return JSON.parse(json)
    }
}
为了防止每次调用feign都执行序列化和请求redis，可以加个本地缓存，将cacheKey缓存起来，设置过期时间，
比如10分钟。对同一个cacheKey10分钟之内只执行一次写缓存。