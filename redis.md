# redis篇
Redis6.0线程模型 https://blog.csdn.net/weixin_38869158/article/details/107504696
线程模型
	Redis在4.0之后，就引入了多线程，比如说除了处理用户命令的主线程之外，还会起异步的线程去做一些资源释放，清理脏数据，删除大key等工作。
	这里我们说之前的Redis是单线程，主要是指原来处理用户命令的线程，是单线程的。
	redis6.0的线程模型，主线程处理用户命令，io线程组用来做网络io。
数据类型
	string、list、hash、set、zset、hyperloglog
编码方式
	hashtable、skiplist、sds、linkedlist、ziplist等
redis事务
	不同于rdbms的acid事务，redis事务没有回滚机制。
渐进式rehash
	hashtable中有两个数组变量，rehash的过程中两个数组都被使用，渐进式的将元素迁移到另一个数组。
key监控
持久化
	aof、rdb
使用lua保证原子性
内存淘汰策略
	Redis中共有下面八种内存淘汰策略：
	volatile-lru：设置了过期时间的key使用LRU算法淘汰；
	allkeys-lru：所有key使用LRU算法淘汰；
	volatile-lfu：设置了过期时间的key使用LFU算法淘汰；
	allkeys-lfu：所有key使用LFU算法淘汰；
	volatile-random：设置了过期时间的key使用随机淘汰；
	allkeys-random：所有key使用随机淘汰；
	volatile-ttl：设置了过期时间的key根据过期时间淘汰，越早过期越早淘汰；
	noeviction：默认策略，当内存达到设置的最大值时，所有申请内存的操作都会报错(如set,lpush等)，只读操作如get命令可以正常执行；
过期key删除策略
	惰性删除、定期删除（每10s随机删除一些过期的key）
集群模式
	redis cluster，redis setinel。集群中各节点一般采用哨兵模式。每个节点被分配不同的slot，存储不同的数据。
	每个节点都是哨兵模式，都有master和slave。
数据分布slot
发布订阅
主从复制
redis主从、哨兵、集群的区别 https://blog.csdn.net/u014761412/article/details/90408173
redlock
	会在超过半数的redis服务器上获取到锁才算真的获取到锁。
	会设置一个看门狗，自动为锁续期。
	需要redis cluster，并且至少5个节点（每个节点只有主没有从，这个集群专门用于分布式锁）。
redis分布式锁（redis是弱一致系统）
	单机。aof和rdb都不能保证数据强一致。单节点挂了，会丢失数据，锁可能丢失。
    主从。主从复制是异步的，虽然可以改配置，在性能和一致性之间做tradeoff，单还是无法强一致。所以一般分布式锁不用主从。
	redlock。

深度图解Redis Cluster原理
https://www.cnblogs.com/detectiveHLH/p/14154665.html
